﻿using System;
using EPiServer.Core;
using EPiServer.DataAnnotations;

namespace EpiserverSite7.Models.Media
{
    [ContentType(GUID = "3bf5cb91-33ae-4b26-8b84-3fb4793ad293")]
    public class GenericMedia : PageData
    {
        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        public virtual String Description { get; set; }
    }
}