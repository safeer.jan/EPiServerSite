﻿using System;
using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.Web;

namespace EpiserverSite7.Models.Blocks
{
    [ContentType(DisplayName = "HomeBlock", GUID = "18325224-a6ad-4ba1-a0e3-c0f736d5ed97", Description = "")]
    public class HomeBlock : BlockData
    {
        [Display(Name = "Main Hero Image",
            GroupName = SystemTabNames.Content,
            Order = 0)]
        [UIHint(UIHint.Image)]
        public virtual ContentReference MainHeroImage { get; set; }

        [Display(Name = "Main Hero Video",
            GroupName = SystemTabNames.Content,
            Order = 0)]
        [UIHint(UIHint.Video)]
        public virtual ContentReference MainHeroVideo { get; set; }

        [CultureSpecific]
        [Display(
            Name = "MainBody",
            Description = "Name field's description",
            GroupName = SystemTabNames.Content,
            Order = 1)]
        public virtual string MainBody { get; set; }

        public virtual ContentArea MainContentArea { get; set; }
    }
}