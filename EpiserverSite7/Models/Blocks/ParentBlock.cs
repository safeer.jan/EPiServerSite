﻿using System;
using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;

namespace EpiserverSite7.Models.Blocks
{
    [ContentType(DisplayName = "ParentBlock", GUID = "3b53bee1-2a1a-48a7-923f-522b9c150b83", Description = "")]
    public class ParentBlock : BlockData
    {
        [CultureSpecific]
        [Display(
            Name = "MainBody Parent",
            Description = "MAinBody field's Parent description",
            GroupName = SystemTabNames.Content,
            Order = 1)]
        public virtual XhtmlString MainBody { get; set; }

        public virtual ContentArea MainContentArea { get; set; }
    }
}