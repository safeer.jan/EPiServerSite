﻿using System;
using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer;

namespace EpiserverSite7.Models.Blocks
{
    [ContentType(DisplayName = "SimpleButtonBlock", GUID = "2dd747fd-2fde-4039-a3c2-7618cd11990e", Description = "")]
    public class SimpleButtonBlock : BlockData
    {
        [CultureSpecific]
        [Display(Order = 1, GroupName = SystemTabNames.Content)]
        [Required]
        public virtual string ButtonText { get; set; }


        [CultureSpecific]
        [Display(Order = 2, GroupName = SystemTabNames.Content)]
        [Required]
        public virtual Url ButtonLink { get; set; }
    }
}