﻿using System;
using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer;
using EPiServer.Web;

namespace EpiserverSite7.Models.Blocks
{
    [ContentType(DisplayName = "Button Block", GUID = "7eb82ba6-77af-4600-8806-7e665c1e3738", Description = "")]
    public class ButtonBlock : BlockData
    {

        [CultureSpecific]
        [Display(
            Name = "ButtonText",
           GroupName = SystemTabNames.Content,
           Order = 0
           )]
        public virtual XhtmlString ButtonText { get; set; }


        //The link must be required as an anchor tag requires an href in order to be valid and focusable

        [CultureSpecific]
        [Display(
            Name = "ButtonLink",
            GroupName = SystemTabNames.Content,
            Order = 1
            )]
        public virtual Url ButtonLink { get; set; }

    }
}