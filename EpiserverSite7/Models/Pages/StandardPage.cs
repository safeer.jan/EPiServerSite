﻿using System;
using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.SpecializedProperties;

namespace EpiserverSite7.Models.Pages
{
    [ContentType(DisplayName = "StandardPage", GUID = "a4fec772-8bb2-4d99-9570-ad2954e38b1a", Description = "")]
   
    public class StandardPage : PageData
    {

        [CultureSpecific]
        [Display(
            Name = "Heading",
            Description = "The page heading.",
            GroupName = SystemTabNames.Content,
            Order = 0)]
        public virtual XhtmlString Heading { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Main body",
            Description = "The main body will be shown in the main content area of the page, using the XHTML-editor you can insert for example text, images and tables.",
            GroupName = SystemTabNames.Content,
            Order = 1)]
        public virtual XhtmlString MainBody { get; set; }

        public virtual ContentArea MainContentArea { get; set; }

    }
}